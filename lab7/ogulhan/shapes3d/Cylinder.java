package ogulhan.shapes3d;
import ogulhan.shapes.Circle;

public class Cylinder extends Circle {
	int height;

	public Cylinder(int h) {
		super(3);
		this.height = h;
	}
	
	public double Volume(){
		return super.Area() * this.height;
	}
	
	public double Area(){
		return 2 * super.Area() + 2 * Math.PI * super.radius * this.height;
		
	}
	
	public String toString(){
		return "Radius = " + super.radius + " Height = " + this.height;
	}
}


