package ogulhan.main;
import ogulhan.shapes.*;
import ogulhan.shapes3d.*;

public class TEST3D {
	public static void main(String[] args){
		
		Cylinder cyl = new Cylinder(4);
		System.out.println(cyl);
		System.out.println(cyl.Area());
		System.out.println(cyl.Volume());
		
		System.out.println("---------------------------------");
		

		Cylinder cy2 = new Cylinder(5);
		System.out.println(cy2);
		System.out.println(cy2.Area());
		System.out.println(cy2.Volume());
		
		System.out.println("---------------------------------");
		

		Cylinder cy3 = new Cylinder(1);
		System.out.println(cy3);
		System.out.println(cy3.Area());
		System.out.println(cy3.Volume());
		
		System.out.println("---------------------------------");
		
		Box bx1 = new Box(6,8,5);
		System.out.println(bx1);
		System.out.println(bx1.Area());
		System.out.println(bx1.Volume());

		System.out.println("---------------------------------");
		
		Box bx2 = new Box(3,6,2);
		System.out.println(bx2);
		System.out.println(bx2.Area());
		System.out.println(bx2.Volume());
	
		System.out.println("---------------------------------");
		
		Box bx3 = new Box(5,7,9);
		System.out.println(bx3);
		System.out.println(bx3.Area());
		System.out.println(bx3.Volume());
	}

}
