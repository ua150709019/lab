package ogulhan.shapes;

public class Circle {
  public int radius;
  
  public Circle(int r){
    radius = r;
  }
  
  public double Area(){
    return Math.PI * radius * radius;
  }
}

