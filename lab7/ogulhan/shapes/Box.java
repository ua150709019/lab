package ogulhan.shapes;

public class Box extends Rectangle {
	int length;
	
	public Box(int l,int height,int width){
		super(width,height);
		this.length = l;
	}
	
	public int Area(){
		return 2 * ( super.Area() + super.width * this.length + super.height * this.length );
	}
	
	public int Volume(){
		return super.Area() * this.length;
	}
	
	public String toString(){
		return "Width = " + super.width + " Height = " + super.height + " Length = " + this.length;
	}
	

}

